# Desafío Sitio Web Estático a S3

Este repositorio contiene los archivos necesarios para completar el desafío de subir un sitio web estático sencillo a un bucket de Amazon S3.

## Descripción del Desafío

El desafío consiste en subir los archivos de un sitio web estático, incluyendo HTML, CS y la carpeta de imagenes, a un bucket de Amazon S3. Esto implica configurar adecuadamente el bucket y sus permisos, así como subir los archivos de manera correcta para que el sitio web sea accesible públicamente.

## Archivos Incluidos

- `index.html`: Página principal del sitio web.
- `style.css`: Hoja de estilos CSS para el sitio web.
- `/imagenes`: Carpeta de imágenes.

## Instrucciones

1. Clona este repositorio en tu máquina local para poder tener los archivos.
2. Sigue el paso a paso del [backlog del reto](https://docs.google.com/spreadsheets/d/1Se4pWClzfLuLJht4jZ4tWSE_GAJz_jheFWHTb-Dff_g/edit#gid=0)


## Notas

- Asegúrate de no incluir información sensible, como claves de acceso, en este repositorio.
- Si tienes problemas o preguntas, no dudes en contactar a tu tutor.

¡Buena suerte!
